from django.test import TestCase,Client
from .models import Schedule
from .views import *
from .FormUser import CreateUserForm,LoginForm
from django.contrib.auth.models import User

# Create your tests here.
class Testing(TestCase):
    #HALAMAN Kegiatan
    def test_cek_kegiatan_template(self):
        response = Client().get('/Activity/')
        self.assertTemplateUsed(response, 'Activity.html')

    def test_cek_url_kegiatan(self):
        response = Client().get('/Activity/')
        self.assertEquals(response.status_code, 200)

    def test_isi_Halaman_kegiatan(self):
        response = Client().get('/Activity/')
        self.assertIn("Activity", response.content.decode('utf8'))
       # self.assertIn("Tambah Kegiatan", response.content.decode('utf8'))
        # self.assertIn("Tambah Peserta", response.content.decode('utf8'))

    #HALAMAN FrontPage
    def test_cek_FrontPage_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'FrontPage.html')

    def test_cek_url_FrontPage(self):
        response = Client().get('/')
        self.assertEquals(response.status_code, 200)

    def test_isi_Halaman_kegiatan(self):
        response = Client().get('/')
        self.assertIn("Rila Bagus Mustofa", response.content.decode('utf8'))
        self.assertIn("Recent Activity", response.content.decode('utf8'))
        self.assertIn("Gallery", response.content.decode('utf8'))
        self.assertIn("Social Media", response.content.decode('utf8'))

    #HALAMAN Bio
    def test_cek_Bio_template(self):
        response = Client().get('/Bio/')
        self.assertTemplateUsed(response, 'Bio.html')

    def test_cek_url_Bio(self):
        response = Client().get('/Bio/')
        self.assertEquals(response.status_code, 200)

    def test_isi_Halaman_Bio(self):
        response = Client().get('/')
        self.assertIn("Bio", response.content.decode('utf8'))

    #HALAMAN Schedule
    def test_cek_Schedule_template(self):
        response = Client().get('/Schedule/')
        self.assertTemplateUsed(response, 'Schedule.html')

    def test_cek_url_Schedule(self):
        response = Client().get('/Schedule/')
        self.assertEquals(response.status_code, 200)

    def test_isi_Halaman_Schedule(self):
        response = Client().get('/')
        self.assertIn("Schedule", response.content.decode('utf8'))
    
    def test_model_exists(self):
        infoMatkul = Schedule.objects.create(matkul="ppw", kelas="A", dos_peng="A", sks="3", ruang="online", desc="semoga lulus")
        self.assertEquals(Schedule.objects.all().count(), 1)

    
    #Halaman Gallery
    def test_cek_Gallery_template(self):
        response = Client().get('/Gallery/')
        self.assertTemplateUsed(response, 'Gallery.html')

    def test_cek_url_Gallery(self):
        response = Client().get('/Gallery/')
        self.assertEquals(response.status_code, 200)
    
    def test_isi_Halaman_Gallery(self):
        response = Client().get('/')
        self.assertIn("Gallery", response.content.decode('utf8'))

    #Halaman Story1
    def test_cek_Story1_template(self):
        response = Client().get('/Story1/')
        self.assertTemplateUsed(response, 'Story1.html')

    def test_cek_url_Story1(self):
        response = Client().get('/Story1/')
        self.assertEquals(response.status_code, 200)

    def test_isi_Halaman_Story1(self):
        response = Client().get('/')
        self.assertIn("My Profile", response.content.decode('utf8'))        

    #Halaman Projects
    def test_cek_Projects_template(self):
        response = Client().get('/Projects/')
        self.assertTemplateUsed(response, 'Projects.html')

    def test_cek_url_Projects(self):
        response = Client().get('/Projects/')
        self.assertEquals(response.status_code, 200)

    def test_isi_Halaman_Projects(self):
        response = Client().get('/')
        self.assertIn("Projects", response.content.decode('utf8'))

    #Add
    def test_cek_Add_template(self):
        response = Client().get('/Add/')
        self.assertTemplateUsed(response, 'Add.html')

    def test_cek_url_Add(self):
        response = Client().get('/Add/')
        self.assertEquals(response.status_code, 200)

    def test_isi_Halaman_Add(self):
        response = Client().get('/Add/')
        self.assertIn("Matkul", response.content.decode('utf8'))
        self.assertIn("Kelas", response.content.decode('utf8'))
        self.assertIn("Dos peng", response.content.decode('utf8'))
        self.assertIn("Sks", response.content.decode('utf8'))
        self.assertIn("Ruang", response.content.decode('utf8'))
        self.assertIn("Desc", response.content.decode('utf8'))

    #HALAMAN Experience
    def test_cek_url_Experience(self):
        response = Client().get('/Experience/')
        self.assertEquals(response.status_code, 200)

    def test_cek_Experience_template(self):
        response = Client().get('/Experience/')
        self.assertTemplateUsed(response, 'Experience.html')

    def test_Index_Title_Exist(self):
        response = Client().get('/Experience/')
        self.assertIn("Experience", response.content.decode('utf8'))
        self.assertIn("Educational Background", response.content.decode('utf8'))
        self.assertIn("Interest", response.content.decode('utf8'))
        self.assertIn("Organization & Comittee", response.content.decode('utf8'))
        self.assertIn("Achievment", response.content.decode('utf8'))

    def test_Button_Exist(self):
        response = Client().get('/Experience/')
        html_response = response.content.decode('utf8')
        self.assertIn("Up", html_response)
        self.assertIn("Down", html_response)

    #Halaman Search
    def test_cek_url_Search(self):
        response = Client().get('/Search/')
        self.assertEquals(response.status_code, 200)

    def test_cek_Search_template(self):
        response = Client().get('/Search/')
        self.assertTemplateUsed(response, 'Search.html')

    #Register Test
    def test_cek_SignUp_url(self):
        response = Client().get('/Register/')
        self.assertEquals(response.status_code,200)

    def test_cek_Register_template_exist(self):
        response = Client().get('/Register/')
        self.assertTemplateUsed(response, 'Register.html')
    
    def test_SignUp(self):
       response = self.client.post('/Register/',follow=True, data={
        'username':'logintest4',
        'email':'test@gmail.com', 
        'password1': 'testpass123', 
        'password2': 'testpass123'
        })
       self.assertContains(response, 'logintest4')

    #Login Test
    def test_cek_SignIn_url(self):
        response = Client().get('/Login/')
        self.assertEquals(response.status_code,200)
    
    def test_template_login_Exist(self):
        response = Client().get('/Login/')
        self.assertTemplateUsed(response, 'Login.html')

    def test_login(self):
        response = self.client.post('/Login/',follow=True, data={
            'username':'logintest4',
            'password': 'testpass123'
            })
        self.assertContains(response, 'logintest4')