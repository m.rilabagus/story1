from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse, request
from .models import *
from .FormAdd import AddMatkul
from .FormKegiatan import FormActivity
from .FormUser import CreateUserForm,LoginForm
from django.contrib.auth.forms import UserCreationForm,AuthenticationForm
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib import messages
import json
import requests

# Create your views here.

def Register(request):
    form = CreateUserForm()

    if request.method == 'POST':
        form = CreateUserForm(data=request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            messages.success(request, f"New account created: {username}")
            return redirect('Story3n4:Login')
        else:
            return redirect('/Register')

    context ={'form':form}
    return render(request, 'Register.html', context)

def Login(request):

    data = LoginForm()

    if request.method == 'POST':
       data = LoginForm(data=request.POST)
       username = request.POST.get('username')
       password =  request.POST.get('password')

       user = authenticate(request, username = username , password = password)
       if user is not None:
           login(request,user)
           return redirect('/')
       else:
            messages.error(request,"Username or password incorrect")
    
    context = {'form' : data}
    return render(request, 'Login.html', context)


def LogoutUser(request):
    logout(request)
    return redirect('Story3n4:Login')

    context ={}
    return render(request, 'Login.html', context)

#@login_required(login_url='Story3n4:Login')
def index(request):
    return render(request, 'FrontPage.html')

def Bio(request):
    return render(request, 'Bio.html')

def Experience(request):
    return render(request, 'Experience.html')

def Gallery(request):
    return render(request, 'Gallery.html')

def Projects(request):
    return render(request, 'Projects.html')

def Detail(request, pk):
    A = Schedule.objects.all()
    det = Schedule.objects.get(id = pk)

    context = {'form':det}
    return render(request, 'Detail.html', context)

def Story1(request):
    return render(request, 'Story1/Story1.html')


def Add(request):

    form = AddMatkul()
    if request.method == 'POST':
        form = AddMatkul(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/Schedule/')

    context = {'form':form}
    return render(request, 'Add.html', context)


def Hapus(request, pk):
    matkul = Schedule.objects.filter(id=pk)
    form = Schedule.objects.get(id= pk)
    if request.method =='POST':
        matkul.delete()
        return redirect('/Schedule/')
    context = {'form':matkul}
    return render(request, 'Hapus.html' , context)

def Jadwal(request):
    schedule = Schedule.objects.all()
    return render(request, 'Schedule.html', {'item' :schedule })

def AddKegiatan(request):
    formAct = FormActivity()
    if request.method == 'POST':
        formAct =FormActivity(request.POST)
        if formAct.is_valid():
            formAct.save()
            return redirect('/Activity/')

    context = {'formAct':formAct}
    return render(request, 'AddKegiatan.html')

def TambahPeserta(request):
    return render(request, 'TambahPeserta.html')

def HapusKegiatan(request):
    return render(request, 'HapusKegiatan.html')

def Aktifitas(request):
    activity = Activity.objects.all()
    return render(request, 'Activity.html',{'item': activity})

def Search(request):
    response = {}
    return render(request, 'Search.html',response)

def Data(request):
    url = "https://www.googleapis.com/books/v1/volumes?q=" + request.GET['q']
    ret = requests.get(url)

    data = json.loads(ret.content)
    return JsonResponse(data, safe = False)


