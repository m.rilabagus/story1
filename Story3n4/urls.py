from django.urls import path
from . import views


app_name = 'Story3n4'

urlpatterns = [
    
    path('',views.index, name='index'),
    path('Bio/', views.Bio, name='Bio'),
    path('Experience/',views.Experience, name='Experience'),
    path('Gallery/', views.Gallery, name='Gallery'),
    path('Projects/', views.Projects, name='Projects'),
    path('Story1/', views.Story1, name='Story1'),
    path('Detail/<pk>/', views.Detail, name='Detail'),
    path('Schedule/', views.Jadwal, name='Jadwal'),
    path('Add/', views.Add, name='Add'),
    path('Hapus/<pk>/', views.Hapus, name='Hapus'),
    path('Activity/', views.Aktifitas, name='Activity'),
    path('TamBahKegiatan/', views.AddKegiatan, name='AddKegiatan'),
    path('HapusKegiatan/', views.HapusKegiatan, name='HapusKegiatan'),
    path('TamBahPeserta/', views.TambahPeserta, name='TambahPeserta'),
    path('Search/', views.Search, name='Search'),
    path('Data/', views.Data, name='Data'),
    path('Register/', views.Register, name='Register'),
    path('Login/', views.Login, name='Login'),
    path('LogoutUser/',views.LogoutUser, name='Logout')

    # dilanjutkan ...
]