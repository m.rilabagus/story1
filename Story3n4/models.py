from django.db import models

# Create your models here.

class Schedule(models.Model):
    matkul = models.CharField(max_length = 200 , null = True)
    kelas = models.CharField(max_length = 200 , null = True)
    dos_peng = models.CharField(max_length = 200 , null = True)
    sks = models.CharField(max_length = 200 , null = True)
    ruang = models.CharField(max_length = 200 , null = True)
    desc = models.CharField(max_length = 200 , null = True)

    def __str__(self):
        return self.matkul

class Activity(models.Model):
    Nama_Kegiatan = models.CharField(max_length = 200 , null = True)
    Peserta = models.CharField(max_length = 200 , null = True)

    def __str__(self):
        return self.Nama_Kegiatan
